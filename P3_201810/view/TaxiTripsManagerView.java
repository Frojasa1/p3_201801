package view;

import java.util.Iterator;
import java.util.Scanner;


import controller.Controller;
import model.data_structures.IList;
import model.logic.TaxiTripsManager;
import model.vo.Camino;
import model.vo.ComponenteConexo;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;
import model.vo.VerticeConServicios;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			
			case 0: // cargar informacion a procesar

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}

				System.out.println("Datos cargados: " + linkJson);
				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.cargarSistema(linkJson);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

					
			case 1: //Req 1
	
				// Mostrar latitud, longitud, total de servicios que salieron y total de servicios que llegaron.
				System.out.println("Informaci�n del v�rtice m�s congestionado: ");
				VerticeConServicios vertice = Controller.R1();
				vertice.print();

				break;

			case 2: //Req 2

				IList<ComponenteConexo> componentes = Controller.R2();
				System.out.println("Componentes fuertemente conexos: " + componentes.size());
				
				int n = 1;
				for (ComponenteConexo componenteConexo : componentes) {
					System.out.println("Componente "+(n++)+":");
					System.out.println("Color: "+componenteConexo.getColor());
					System.out.println("N�mero de vertices: "+componenteConexo.getNumVertices());
				}
			
				break;

			case 3: //Req 3

				
				try {
					Controller.R3();
					System.out.println("Mapa cargado exitosamente");
				} catch (Exception e) {
					System.err.println("Problemas cargando mapa");
					break;
				}


				break;

			case 4: //Req 4
				
				Camino camino = Controller.R4();
				System.out.println("Camino de costo minimo: ");
				camino.print();
				
				

				break;

			case 5: //Req 5
				
				Camino[] mayor_menor = Controller.R5();
				
				System.out.println("Camino de duracion mayor: ");
				mayor_menor[0].print();
				System.out.println("-------------------------------------------------");
				System.out.println("Camino de duracion menor: ");
				mayor_menor[1].print();
				
				break;

			case 6: //Req 6
				
				IList<Camino> caminos = Controller.R6();
				System.out.println("Caminos sin peaje: ");
				if(caminos.size()==0)
					System.out.println("No hay caminos sin peaje entre esos puntos");
				for (Camino camino2 : caminos) {
					camino2.printSimple();
				}
			
				break;

			case 7: 
				fin=true;
				sc.close();
				break;

			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 2----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("0. Cargar toda la informacion del sistema de una fuente de datos (small, medium o large).");

		System.out.println("1. Obtener v�rtice m�s congestionado en Chicago");
		System.out.println("2. Obtener componentes fuertemente conexos");
		System.out.println("3. Generar mapa coloreado de la red vial de Chicago en Google Maps");
		System.out.println("4. Encontrar camino de menor distancia para dos puntos aleatorios");
		System.out.println("5. Hallar caminos de mayor y menor duracion entre dos puntos aleatorios");
		System.out.println("6. Encontrar caminos sin peaje entre dos puntos aleatorios");
		System.out.println("7. Salir");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Cargando archivo de calles en './data/StreetLines.csv' ");
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Ingrese el numero de la fuente a cargar y presione <Enter> para confirmar: (e.g., 1)");
	}

}
