package test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Arco;
import model.data_structures.Vertice;
import model.data_structures.WeightedDirectedGraph;

public class WDGTest <Key extends Comparable<Key> ,Value>
{
	private WeightedDirectedGraph WDG;
	/**
     * Crea una instancia de la clase WeightedDirectedGraph. Este m�todo se ejecuta antes de cada m�todo de prueba.
     */
	@Before
    public void setupEscenario1( )
    {
		WDG=new WeightedDirectedGraph<>();
    }
    
	
	@Test
    public void test1()
    {
		setupEscenario1();
    	assertTrue("El grafo no est� correctamente creado", WDG.numVertices()==0);
    }
	
	@Test
    public void test2()
    {
		setupEscenario1();
    	assertTrue("El grado del grafo no es correcto", WDG.darGrado(0)==-1);
    }
    
}
