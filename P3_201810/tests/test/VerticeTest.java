package test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Arco;
import model.data_structures.Vertice;

public class VerticeTest <Key extends Comparable< Key > , Value>
{
	private Vertice vertice;
	/**
     * Crea una instancia de la clase Vertice. Este m�todo se ejecuta antes de cada m�todo de prueba.
     */
	@Before
    public void setupEscenario1( )
    {
		Value value=(Value) "Prueba";
		Key llave=(Key) "llave";
		vertice=new Vertice(value, llave);
    }
    
	
	@Test
    public void test1()
    {
    	assertTrue("El valor del vertice no es correcto", vertice.getValor().equals("Prueba"));
    	assertTrue("La llave del vertice no es correcto", vertice.getLlave().equals("llave"));
    	assertTrue("No se ha cargado correctamente la informaci�n", vertice.getMark()==false);
    	assertTrue("La lista del vertice no se ha cargado correctamente", vertice.getArcos().darNumeroElementos()==0);
    }
	
	
    
}
