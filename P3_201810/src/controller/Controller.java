package controller;

import API.ITaxiTripsManager;
import model.data_structures.IList;
import model.logic.TaxiTripsManager;
import model.vo.Camino;
import model.vo.ComponenteConexo;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;


import model.vo.VerticeConServicios;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager =new TaxiTripsManager();

	//Carga El sistema (Requerimiento 0)
	public static boolean cargarSistema(String direccionJson)
	{
		return manager.cargarSistema(direccionJson);
	}
	//Req 1
	public static VerticeConServicios R1()
	{
		return manager.Req1VerticeMasCongestionado();
	}

	//Req 2
	public static IList<ComponenteConexo> R2()
	{
		return manager.Req2ComponentesConexos();
	}

	//Req 3
	public static void R3()
	{
		manager.Req3GenerarMapaComponentes();
	}

	//Req 4
	public static Camino R4()
	{
		return manager.Req4CaminoCostoMinimo();
	}	
	//Req 5
	public static Camino[] R5()
	{
		return manager.Req5Caminos_Mayor_MenorDuracion();
	}	
	//Req 6
	public static IList<Camino> R6()
	{
		return manager.Req6CaminosSinPeaje();
	}	
}
