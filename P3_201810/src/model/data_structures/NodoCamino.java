package model.data_structures;

public class NodoCamino< Key > {

	private Key origen ;
	private double costo=0;
	private ListaEncadenada<Key> camino ;
	
	public NodoCamino(){
		camino= new ListaEncadenada<>();
	}
	
	public Key getOrigen() {
		return origen;
	}

	public void setOrigen(Key origen) {
		this.origen = origen;
	}

	public double getCosto() {
		return costo;
	}

	public void setCosto(int costo) {
		this.costo = costo;
	}

	public ListaEncadenada<Key> getCamino() {
		return camino;
	}

	public void setCamino(ListaEncadenada<Key> camino) {
		this.camino = camino;
	}
	
	public void agregarVertice( Key nuevo, double pcosto) {
		costo+= pcosto;
		camino.agregarElementoFinal(nuevo);
	}
	

}
