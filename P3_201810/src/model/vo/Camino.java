package model.vo;

import model.data_structures.IList;

public class Camino implements Comparable<Camino> {
	private IList<VerticeConServicios> vertices;
	private long tiempo;
	private double distancia;
	private double valor;
	public Camino(long tiempo, double distancia, double valor) {
		this.tiempo = tiempo;
		this.distancia = distancia;
		this.valor = valor;
//		Inicializar lista de vertices
	}
	public long getTiempo() {
		return tiempo;
	}
	public void setTiempo(long tiempo) {
		this.tiempo = tiempo;
	}
	public double getDistancia() {
		return distancia;
	}
	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	public IList<VerticeConServicios> getVertices(){
		return this.vertices;
	}
	
	public void agregarVertice(VerticeConServicios vertice){
		vertices.add(vertice);
	}
	
	public void printSimple(){
		//Tiempo estimado
		System.out.println("Tiempo estimado: " + this.getTiempo());
		//Distancia estimada
		System.out.println("Distancia estimada: " + this.getDistancia());
		//Valor estimado a pagar
		System.out.println("Valor estimado: " + this.getValor());
	}
	
	public void print(){
		System.out.println("Listado de v�rtices a seguir: ");
		//Lista de vertices
		for (VerticeConServicios vertice1 : vertices) {
			System.out.println("Latitud: " + vertice1.getLatRef()+". Longitud: "+vertice1.getLongRef());
		}
		
		//Tiempo estimado
		System.out.println("Tiempo estimado: " + this.getTiempo());
		//Distancia estimada
		System.out.println("Distancia estimada: " + this.getDistancia());
		//Valor estimado a pagar
		System.out.println("Valor estimado: " + this.getValor());
	}
	@Override
	public int compareTo(Camino o) {
		int rta = Long.compare(this.tiempo, o.getTiempo());
		if(rta==0)
			rta = Double.compare(this.valor, o.getValor());
		return rta;
	}
	

}
