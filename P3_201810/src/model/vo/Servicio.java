package model.vo;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Representation of a Service object
 */
public class Servicio {
	
String idServicio;
String idTaxi;
Date fechaInicio ;
Date fechaFinal ;
Double distancia;
Integer duracion;
Integer areaSalida;
Integer areaLlegada;
Double dinero;
Double longitud;
Double latitud;

	

public Servicio(String pservicio , String taxi , Date inicio , Date ffinal , Double dista , Integer dura , int salida , int lleghada, Double pdinero,Double plong, Double plat)
{
	idServicio=pservicio;
	idTaxi=taxi;
	fechaInicio=inicio;
	fechaFinal=ffinal;
	distancia=dista;
	duracion=dura;
	areaSalida=salida;
	areaLlegada= lleghada;
	dinero=pdinero;
	longitud=plong;
	latitud=plat;
}


public Double getLongitud() {
	return longitud;
}


public void setLongitud(Double longitud) {
	this.longitud = longitud;
}


public Double getLatitud() {
	return latitud;
}


public void setLatitud(Double latitud) {
	this.latitud = latitud;
}


public Double getDinero() {
	return dinero;
}

public void setDinero(Double dinero) {
	this.dinero = dinero;
}


public String getIdServicio() {
	return idServicio;
}



public void setIdServicio(String idServicio) {
	this.idServicio = idServicio;
}



public String getIdTaxi() {
	return idTaxi;
}



public void setIdTaxi(String idTaxi) {
	this.idTaxi = idTaxi;
}



public Date getFechaInicio() {
	return fechaInicio;
}



public void setFechaInicio(Timestamp fechaInicio) {
	this.fechaInicio = fechaInicio;
}



public Date getFechaFinal() {
	return fechaFinal;
}



public void setFechaFinal(Timestamp fechaFinal) {
	this.fechaFinal = fechaFinal;
}



public Double getDistancia() {
	return distancia;
}



public void setDistancia(double distancia) {
	this.distancia = distancia;
}



public double getDuracion() {
	return duracion;
}



public void setDuracion(Integer duracion) {
	this.duracion = duracion;
}



public int getAreaSalida() {
	return areaSalida;
}



public void setAreaSalida(int areaSalida) {
	this.areaSalida = areaSalida;
}



public int getAreaLlegada() {
	return areaLlegada;
}



public void setAreaLlegada(int areaLlegada) {
	this.areaLlegada = areaLlegada;
}


}
