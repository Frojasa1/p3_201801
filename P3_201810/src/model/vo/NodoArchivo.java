package model.vo;


public class NodoArchivo 
{
	private String nodeId;
	private String streetName;
	private String streetType;
	private String preDir;
	private String oneWay;
	private String theGeom;
	private String theGeom2;
	
	public NodoArchivo(String node, String streetN, String streetT, String preD, String One, String TheG, String TheG2)
	{
		setNodeId(node);
		setStreetName(streetN);
		setStreetType(streetT);
		setPreDir(preD);
		setOneWay(One);
		setTheGeom(TheG);
		setTheGeom2(TheG2);
	}

	/**
	 * @return the nodeId
	 */
	public String getNodeId() {
		return nodeId;
	}

	/**
	 * @param nodeId the nodeId to set
	 */
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	/**
	 * @return the streetName
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * @param streetName the streetName to set
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	/**
	 * @return the streetType
	 */
	public String getStreetType() {
		return streetType;
	}

	/**
	 * @param streetType the streetType to set
	 */
	public void setStreetType(String streetType) {
		this.streetType = streetType;
	}

	/**
	 * @return the preDir
	 */
	public String getPreDir() {
		return preDir;
	}

	/**
	 * @param preDir the preDir to set
	 */
	public void setPreDir(String preDir) {
		this.preDir = preDir;
	}

	/**
	 * @return the oneWay
	 */
	public String getOneWay() {
		return oneWay;
	}

	/**
	 * @param oneWay the oneWay to set
	 */
	public void setOneWay(String oneWay) {
		this.oneWay = oneWay;
	}

	/**
	 * @return the theGeom
	 */
	public String getTheGeom() {
		return theGeom;
	}

	/**
	 * @param theGeom the theGeom to set
	 */
	public void setTheGeom(String theGeom) {
		this.theGeom = theGeom;
	}

	/**
	 * @return the theGeom2
	 */
	public String getTheGeom2() {
		return theGeom2;
	}

	/**
	 * @param theGeom2 the theGeom2 to set
	 */
	public void setTheGeom2(String theGeom2) {
		this.theGeom2 = theGeom2;
	}
}
