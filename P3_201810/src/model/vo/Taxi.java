package model.vo;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;

import model.data_structures.ColaPrioridad;
import model.data_structures.ListaEncadenada;
import model.data_structures.RedBlackBST;

import model.data_structures.ListaEncadenada;
import model.data_structures.RedBlackBST;

public class Taxi<Sevicio> implements Comparable<Taxi> {
	
	String idTaxi;
	String compania;
	Double puntos;
	ListaEncadenada<Servicio> servicios;
	RedBlackBST<Date, Servicio> seviciosCrono ; 

	public Taxi(String taxi , String compa){
		idTaxi=taxi;
		compania=compa;
		servicios= new ListaEncadenada<>();
		seviciosCrono = new RedBlackBST<>();
	}

	public String getIdTaxi() {
		return idTaxi;
	}

	public void setIdTaxi(String idTaxi) {
		this.idTaxi = idTaxi;
	}

	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}
	
	
	
	public ListaEncadenada<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(ListaEncadenada<Servicio> servicios) {
		this.servicios = servicios;
	}

	public RedBlackBST<Date, Servicio> getSeviciosCrono() {
		return seviciosCrono;
	}

	public void setSeviciosCrono(RedBlackBST<Date, Servicio> seviciosCrono) {
		this.seviciosCrono = seviciosCrono;
	}
	
	
	

	public void setPuntos(Double puntos) {
		this.puntos = puntos;
	}

	@Override
	public int compareTo(Taxi arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	



	
}
